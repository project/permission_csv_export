<?php

/**
 * @file
 * Module permission_csv_export file.
 */

const PERMISSION_CSV_EXPORT_FILEPATH = 'public://perm_csv/';
const PERMISSION_CSV_EXPORT_FILETYPE = 'csv';
const PERMISSION_CSV_EXPORT_FILENAME = 'permissions_csv_export';
const PERMISSION_CSV_EXPORT_BACKUP_FILENAME = 'permissions_backup';
const PERMISSION_CSV_EXPORT_IMPORTED = 0;
const PERMISSION_CSV_EXPORT_PROCESSED = 1;
const PERMISSION_CSV_EXPORT_DECLINED = 2;
const PERMISSION_CSV_EXPORT_DEFAULT_MENU = 'admin/config/people/export-permission-csv';
const PERMISSION_CSV_EXPORT_MENU = 'admin/config/people/export-permission-csv/export';
const PERMISSION_CSV_EXPORT_IMPORT_MENU = 'admin/config/people/export-permission-csv/import';
const PERMISSION_CSV_EXPORT_LOGS_MENU = 'admin/config/people/export-permission-csv/import-logs';
const PERMISSION_CSV_EXPORT_REVERT_BACKUP_MENU = 'admin/config/people/export-permission-csv/revert';
const PERMISSION_CSV_EXPORT_DELETE_LOGS = 'admin/config/people/export-permission-csv/delete-all-logs';
const PERMISSION_CSV_EXPORT_IMPORT_PROCESS_LIMIT = 100;

/**
 * Implements hook_help().
 */
function permission_csv_export_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/help#permission_csv_export':
      $output = '';
      $output .= '<h3>' . t( 'About' ) . '</h3>';
      $output .= '<p>' . t( 'This module allow user to export and import permissions using CSV, so administrator or authorized user can download the role wise permissions into csv, he can export it for selected roles.' ) . '</p>';
      $output .= '<p>' . t( 'Then they can change permissions directly into CSV as for larger sites when there are lots of permissions so they can not view properly and some times form does not work properly so they can change permissions without taking so much of risks.' ) . '</p>';
      $output .= '<p>' . t( 'Then they can import that altered file and process it at any time, and new permission will get imported into the system, although it will create an automatic backup of last active permissions on the site.' ) . '</p>';
      $output .= '<p>' . t( 'They can also view the logs of files which was imported by the user along with date and status.' ) . '</p>';
      $output .= '<p>' . t( 'If for some reasons you want to revert to old permissions then you can also do that.' ) . '</p>';

      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function permission_csv_export_permission() {
  return [
    'administer permission csv export' => [
      'title' => t( 'Administer permission csv export' ),
      'description' => t( 'Perform administration tasks for permission csv export.' ),
    ],
  ];
}

/**
 * Implements hook_menu().
 */
function permission_csv_export_menu() {
  $items[PERMISSION_CSV_EXPORT_DEFAULT_MENU] = [
    'title' => 'Permissions Export/Import into CSV',
    'description' => 'Page to export roles and their associated permissions',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['permission_csv_export_form'],
    'access arguments' => ['administer users'],
    'file' => 'includes/export.admin.inc',
  ];
  $items[PERMISSION_CSV_EXPORT_MENU] = [
    'title' => 'Export',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  ];
  $items[PERMISSION_CSV_EXPORT_IMPORT_MENU] = [
    'title' => 'Import',
    'description' => 'Page to import roles and their associated permissions',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['permission_csv_export_import_form'],
    'access arguments' => ['administer users'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/import.admin.inc',
  ];
  $items[PERMISSION_CSV_EXPORT_LOGS_MENU] = [
    'title' => 'Import File logs',
    'description' => 'Display import files and related information.',
    'page callback' => 'permission_csv_export_file_logs',
    'access arguments' => ['administer users'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/permission_csv_export.inc',
  ];
  $items[PERMISSION_CSV_EXPORT_REVERT_BACKUP_MENU] = [
    'title' => 'Revert to last Backup',
    'description' => 'Revert to last active permissions.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['permission_csv_export_revert_permissions_form'],
    'access arguments' => ['administer users'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/permission_csv_export.inc',
  ];
  $items[PERMISSION_CSV_EXPORT_DELETE_LOGS] = [
    'title' => 'Delete All imported files Logs',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['permission_csv_export_log_delete_confirm'],
    'access arguments' => ['administer users'],
    'type' => MENU_CALLBACK,
    'file' => 'includes/permission_csv_export.inc',
  ];

  return $items;
}

/**
 * Get file uri from file fid.
 *
 * @param int $fid
 *   File fid.
 *
 * @return string
 *   URI fo the file.
 */
function permission_csv_export_get_file_uri_from_fid($fid) {
  return db_query( "SELECT uri FROM {file_managed} WHERE fid = :fid", [
    ':fid' => $fid,
  ] )->fetchField();
}

/**
 * Get unprocessed file from logs.
 *
 * @return int
 *   file fid.
 */
function permission_csv_export_get_unprocessed_file() {
  return db_query( "SELECT fid, import_roles FROM {permissions_csv_export_logs} WHERE status = :status", [
    ':status' => PERMISSION_CSV_EXPORT_IMPORTED,
  ] )->fetchAll();
}

/**
 * Get the file status from status id.
 *
 * @param int $status
 *   Status code of the file.
 *
 * @return string
 *   Readable status of the file.
 */
function permission_csv_export_get_file_status($status) {
  return ($status == 0) ? 'Unprocessed' : (($status == 1) ? 'Processed' : 'Declined');
}

/**
 * Helper function to get rolewise permission access.
 *
 * @param string $perms
 *   Name of the permission.
 * @param int $rid
 *   Role id.
 * @param string $role
 *   Role Name.
 * @param array $modules
 *   Permission array from all enabled modules.
 * @param array $role_permission
 *   Associative array of role and permissions.
 */
function permission_csv_export_get_rolewise_permission_access($perms, $rid, $role, $modules, &$role_permission) {
  foreach ($perms[$rid] as $perm => $val) {
    $role_permission[$perm][$role] = array_key_exists( $perm, $modules ) ? 1 : 0;
  }
}

/**
 * Get number of results from a given table.
 *
 * @param string $table
 *   Table name.
 *
 * @return int
 *   Number of results.
 */
function permission_csv_export_get_number_of_results($table) {
  return db_select( $table )
    ->fields( NULL, ['field'] )
    ->countQuery()
    ->execute()
    ->fetchField();
}

/**
 * Get allowed roles for permissions import.
 *
 * @param int $fid
 *   File fid.
 *
 * @return string
 *   Serialized string of roles.
 */
function permission_csv_export_get_allowed_roles_for_import($fid) {
  return db_select( 'permissions_csv_export_logs' )
    ->fields( 'permissions_csv_export_logs', ['import_roles'] )
    ->condition( 'fid', $fid )
    ->execute()
    ->fetchField();
}

/**
 * Get range of data from given table.
 *
 * @param string $table
 *   Table name.
 * @param int $start
 *   Start Limit.
 * @param int $end
 *   End Limit.
 *
 * @return Object
 *   Object of all data.
 */
function permission_csv_export_get_results_from_table($table, $start, $end) {
  $query = db_select( $table );
  $query->fields( $table );
  $query->range( $start, $end );

  return $query->execute()->fetchAll();
}

/**
 * Generate CSV from data.
 *
 * @param array $csv_data
 *   Data of CSV.
 * @param string $filename
 *   File name of output file.
 */
function permission_csv_export_generate_csv($csv_data, $filename) {
  $filename .= '_' . date( 'dmYhmi', time() ) . '.csv';
  // Output headers so that the file is downloaded rather than displayed.
  header( 'Content-Type: text/csv; charset=utf-8' );
  header( 'Content-Disposition: attachment; filename=' . $filename );

  // Create a file pointer connected to the output stream.
  $output = fopen( 'php://output', 'w' );
  // Output the column headings.
  foreach ($csv_data as $csv_row) {
    fputcsv( $output, $csv_row );
  }
  exit();
}
