<?php

/**
 * @file
 * Menu callbacks and other related code.
 */

/**
 * Menu callback for csv import file logs.
 *
 * @return string
 *   Table formatted output in html.
 */
function permission_csv_export_file_logs() {
  $roles = user_roles();
  $header = [
    'S.N.',
    'Filename',
    'Imported By',
    'Status',
    'Processed on',
    'Import on',
    'Imported for',
    'Download',
  ];
  $data = [];
  $result = db_query( "SELECT id, fid, uid, import_roles, status, created, updated FROM {permissions_csv_export_logs} order by updated" )->fetchAll();
  if (count( $result )) {
    $i = 1;
    foreach ($result as $value) {
      $url = (($uri = permission_csv_export_get_file_uri_from_fid( $value->fid )) && file_exists( drupal_realpath( $uri ) )) ? urldecode( file_create_url( $uri ) ) : '';
      $username = db_query( "select name from {users} where uid = :uid", [':uid' => $value->uid] )->fetchField();
      $status = permission_csv_export_get_file_status( $value->status );
      $download = l( t( 'Download' ), $url, ['attributes' => ['target' => '_blank']] );
      $imported_for = unserialize( $value->import_roles );
      foreach ($imported_for as $k => $rid) {
        $imported_for[$k] = $roles[$rid];
      }
      $data[] = [
        $i,
        basename( $url ),
        $username,
        $status,
        date( 'm-d-Y h:i:s', $value->created ),
        date( 'm-d-Y h:i:s', $value->updated ),
        implode( "<br />", $imported_for ),
        $download,
      ];
      $i++;
    }
    $output = '<div class="action-links"><p>' . l( t( "Delete All Logs" ), PERMISSION_CSV_EXPORT_DELETE_LOGS ) . '</p></div>';
    $output .= theme( 'table', ['header' => $header, 'rows' => $data] );

    return $output;
  }

  return "<h3>There is no import log available right now.</h3>";
}

/**
 * Logs delete confirm form.
 *
 * @see permission_csv_export_log_delete_confirm_submit
 */
function permission_csv_export_log_delete_confirm($form) {
  return confirm_form( $form, t( 'Are you sure you want to delete all logs, remember you will not able to see any logs after this.' ), PERMISSION_CSV_EXPORT_LOGS_MENU, t( 'This action cannot be undone.' ), t( 'Delete' ), t( 'Cancel' ) );
}

/**
 * Submit callback for logs confirm delete form.
 *
 * @see permission_csv_export_log_delete_confirm_validate
 */
function permission_csv_export_log_delete_confirm_submit($form, &$form_state) {
  db_truncate( 'permissions_csv_export_logs' )->execute();
  drupal_set_message( t( 'All import csv logs deleted successfully.' ) );
  drupal_goto( PERMISSION_CSV_EXPORT_LOGS_MENU );
}

/**
 * Form constructor for revert permissions form.
 *
 * @see permission_csv_export_revert_permissions_form_submit()
 *
 * @ingroup forms
 */
function permission_csv_export_revert_permissions_form($form, &$form_state) {
  $is_backup_exist = permission_csv_export_get_number_of_results( 'permissions_csv_export_last_bakup' );
  $form['pce'] = [
    '#type' => 'fieldset',
    '#title' => t( 'Revert last active permissions' ),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];
  if ($is_backup_exist) {
    $form['pce']['info'] = [
      '#markup' => "<p>If you want to revert the last active permissions then please click on 'Revert Permissions', and if you want to download the last active permissions then click on 'Download as CSV'.</p>",
    ];
    $form['pce']['submit'] = [
      '#type' => 'submit',
      '#value' => t( 'Revert Permissions' ),
    ];
    $form['pce']['download'] = [
      '#type' => 'submit',
      '#value' => t( 'Download as CSV' ),
    ];
  }
  else {
    $form['pce']['info'] = [
      '#markup' => "<p>There is no backup found in the system right now.</p>",
    ];
  }

  return $form;
}

/**
 * Submit callback of permission revert form.
 *
 * @see permission_csv_export_revert_permissions_form
 */
function permission_csv_export_revert_permissions_form_submit($form, $form_state) {
  $results = permission_csv_export_get_number_of_results( 'permissions_csv_export_last_bakup' );
  if ($form_state['values']['op'] == 'Download as CSV') {
    $roles = user_roles();
    if (count( $results )) {
      $csv_header = ['permission'];
      $query = db_select( 'permissions_csv_export_last_bakup' );
      $query->fields( 'permissions_csv_export_last_bakup', ['rids'] );
      $query->range( 0, 1 );
      $rids = array_keys( unserialize( $query->execute()->fetchField() ) );
      foreach ($rids as $value) {
        $csv_header = array_merge( $csv_header, [$roles[$value]] );
      }
      $csv_data = [];
      $query = db_select( 'permissions_csv_export_last_bakup' );
      $query->fields( 'permissions_csv_export_last_bakup' );
      $rows = $query->execute()->fetchAll();
      foreach ($rows as $value) {
        $row_data = [$value->permission];
        $rids = unserialize( $value->rids );
        foreach ($csv_header as $key => $header) {
          if ($header == 'permission') {
            continue;
          }
          $row_data[$key] = $rids[array_search( $header, $roles )];
        }
        $csv_data[] = $row_data;
      }
      permission_csv_export_generate_csv( array_merge( [$csv_header], $csv_data ), PERMISSION_CSV_EXPORT_BACKUP_FILENAME );
    }
  }
  else {
    for ($i = 0; $i < $results;) {
      $start = $i;
      $i = ($results < ($i + PERMISSION_CSV_EXPORT_IMPORT_PROCESS_LIMIT)) ? $results : $i + PERMISSION_CSV_EXPORT_IMPORT_PROCESS_LIMIT;
      $rows = permission_csv_export_get_results_from_table( 'permissions_csv_export_last_bakup', $start, $i );
      $operations_permissions[] = [
        'permission_csv_export_revert_permissions_batch_processing',
        [$rows],
      ];
    }
    db_truncate( 'permissions_csv_export_last_bakup' )->execute();
    // Once everything is gathered and ready to be processed.
    $batch = [
      'title' => t( 'Reverting Permissions...' ),
      'operations' => $operations_permissions,
      'finished' => 'permission_csv_export_revert_permissions_batch_processing_finished',
      'error_message' => t( 'The revert permissions has encountered an error.' ),
      'progress_message' => t( 'Reverted @current of @total rows.' ),
      'file' => drupal_get_path( 'module', 'permission_csv_export' ) . '/includes/permission_csv_export.inc',
    ];
    batch_set( $batch );
    db_truncate( 'permissions_csv_export_data' )->execute();
  }
}

/**
 * Batch function to revert permissions.
 *
 * @see permission_csv_export_revert_permissions_form_submit()
 */
function permission_csv_export_revert_permissions_batch_processing($data) {
  if (count( $data )) {
    $rid_revoke = [];
    $rid_grant = [];
    foreach ($data as $value) {
      $rids = unserialize( $value->rids );
      foreach ($rids as $rid => $access) {
        if ($access == 'Y' || $access == 'y' || $access == 1) {
          $rid_grant[$rid][] = $value->permission;
        }
        if ($access == 'N' || $access == 'n' || $access == 0) {
          $rid_revoke[$rid][] = $value->permission;
        }
      }
    }
    foreach ($rid_revoke as $rid => $permissions) {
      user_role_revoke_permissions( $rid, $permissions );
    }
    foreach ($rid_grant as $rid => $permissions) {
      user_role_grant_permissions( $rid, $permissions );
    }
  }
}

/**
 * This function runs when the batch processing will completed.
 *
 * @see permission_csv_export_revert_permissions_form_submit()
 */
function permission_csv_export_revert_permissions_batch_processing_finished() {
  drupal_set_message( t( 'Permissions reverted Successfully' ) );
}
