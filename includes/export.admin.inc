<?php

/**
 * @file
 * Export role(s) and their associated permissions into csv.
 */

/**
 * Form constructor for export roles permissions in csv.
 *
 * @see permission_csv_export_form_validate()
 * @see permission_csv_export_form_submit()
 *
 * @ingroup forms
 */
function permission_csv_export_form($form, &$form_state) {
  $roles = user_roles();
  $form['pce'] = [
    '#type' => 'fieldset',
    '#title' => t( 'Export permissions' ),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];
  if (count( $roles )) {
    $form['pce']['roles'] = [
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#title' => t( 'Available Roles' ),
      '#description' => t( 'Select roles for which you want to export permissions.' ),
    ];
    $form['pce']['submit'] = [
      '#type' => 'submit',
      '#value' => t( 'Export Permissions' ),
    ];
  }
  else {
    $form['pce']['no_role'] = [
      '#type' => 'item',
      '#markup' => t( 'No role exists.' ),
    ];
  }

  return $form;
}

/**
 * Use to validate user must be selected at least one role to export permissios.
 *
 * @see permission_csv_export_form_submit()
 */
function permission_csv_export_form_validate($form, $form_state) {
  $roles = $form_state['values']['roles'];
  $flag = FALSE;
  foreach ($roles as $data) {
    if ($data) {
      $flag = TRUE;
      break;
    }
  }

  if ($flag == FALSE) {
    form_set_error( 'roles', t( 'Please select at least one role.' ) );
  }
}

/**
 * Submit callback of export form.
 *
 * @see permission_csv_export_form_validate()
 */
function permission_csv_export_form_submit($form, $form_state) {
  $roles = user_roles();
  $modules = user_permission_get_modules();
  $csv_header = ['permission'];
  $csv_data = [];
  $role_permission = [];
  foreach ($form_state['values']['roles'] as $rid => $value) {
    if ($value) {
      $csv_header = array_merge( $csv_header, [$roles[$rid]] );
      $perms = user_role_permissions( [$rid => $rid] );
      permission_csv_export_get_rolewise_permission_access( $perms, $rid, $roles[$rid], $modules, $role_permission );
    }
  }
  permission_csv_export_generate_csv_data( $modules, $role_permission, $csv_header, $csv_data );
  permission_csv_export_generate_csv( array_merge( [$csv_header], $csv_data ), PERMISSION_CSV_EXPORT_FILENAME );
}

/**
 * Helper function to generate csv data.
 *
 * @param array $modules
 *   Permission array from all active modules.
 * @param array $role_permission
 *   Associative array of rolewise permissions.
 * @param array $csv_header
 *   Header array of CSV.
 * @param array $csv_data
 *   Data of CSV.
 */
function permission_csv_export_generate_csv_data($modules, $role_permission, $csv_header, &$csv_data) {
  foreach ($modules as $permission => $v) {
    $row = [$permission];
    for ($i = 1; $i < count( $csv_header ); $i++) {
      $access = isset( $role_permission[$permission][$csv_header[$i]] ) ? 'Y' : 'N';
      $row = array_merge( $row, [$access] );
    }
    $csv_data[] = $row;
  }
}
