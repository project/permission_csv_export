<?php

/**
 * @file
 * Import role(s) and their associated permissions into csv.
 */

/**
 * Form constructor for import roles permissions in csv.
 *
 * @see permission_csv_export_import_form_submit()
 * @see permission_csv_export_import_form_validate()
 *
 * @ingroup forms
 */
function permission_csv_export_import_form($form, &$form_state) {
  $roles = user_roles();
  $unprocessed_file = permission_csv_export_get_unprocessed_file();
  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['pce'] = [
    '#type' => 'fieldset',
    '#title' => t( 'Import permissions' ),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];
  if (count( $roles ) && empty( $unprocessed_file )) {
    $form['pce']['file'] = [
      '#type' => 'managed_file',
      '#title' => t( 'CSV File' ),
      '#description' => t( 'Upload role and permission file in CSV format.' ),
      '#upload_location' => PERMISSION_CSV_EXPORT_FILEPATH,
      '#upload_validators' => [
        'file_validate_extensions' => [PERMISSION_CSV_EXPORT_FILETYPE],
      ],
      '#element_validate' => ['permission_csv_export_import_file_validate'],
    ];
    $form['pce']['roles'] = [
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#title' => t( 'Available Roles' ),
      '#description' => t( 'Select roles for which you want to import permissions, if you want to import permissions for all roles then leave it blank.' ),
    ];
    $form['pce']['submit'] = [
      '#type' => 'submit',
      '#value' => t( 'Import Csv File' ),
    ];
  }
  else {
    $unprocessed_file = reset( $unprocessed_file );
    $file = urldecode( file_create_url( permission_csv_export_get_file_uri_from_fid( $unprocessed_file->fid ) ) );
    $file_info = l( basename( $file ), $file, ['attributes' => ['target' => '_blank']] );
    $role_info = [];
    foreach (unserialize( $unprocessed_file->import_roles ) as $role) {
      $role_info[] = $roles[$role];
    }
    $role_info = implode( ", ", $role_info );
    $form['pce']['process'] = [
      '#type' => 'hidden',
      '#value' => 1,
    ];
    $form['pce']['fid'] = [
      '#type' => 'hidden',
      '#value' => $unprocessed_file->fid,
    ];
    $form['pce']['info'] = [
      '#markup' => "<p>Please click on 'Import Permissions' button to process csv file, remember it will change permissions of selected roles which is mentioned below, it will create an automatic backup of your existing permissions so you can revert back to permissions at any time. If you want to import new file then please click on 'Discard CSV' button.</p>",
    ];
    $form['pce']['csv_file'] = [
      '#markup' => '<p><b>File Name: ' . $file_info . '</b></p>',
    ];
    $form['pce']['roles_imported'] = [
      '#markup' => '<p><b>Imported for: ' . $role_info . '</b></p>',
    ];
    $form['pce']['submit'] = [
      '#type' => 'submit',
      '#value' => t( 'Import Permissions' ),
    ];
    $form['pce']['discard'] = [
      '#type' => 'submit',
      '#value' => t( 'Discard CSV' ),
    ];
  }

  return $form;
}

/**
 * Use to validate csv file field.
 *
 * @see permission_csv_export_import_file_submit
 */
function permission_csv_export_import_file_validate($element, $form_state) {
  if ($form_state['clicked_button']['#value'] != 'Remove') {
    // Get the uri of uploaded file.
    $uri = permission_csv_export_get_file_uri_from_fid( $form_state['values']['file']['fid'] );
    if (!empty( $uri )) {
      if (file_exists( drupal_realpath( $uri ) )) {
        $csv_header = permission_csv_export_get_allowed_csv_header();
        // Open the csv.
        $handle = fopen( drupal_realpath( $uri ), "r" );
        $header = fgetcsv( $handle, 4096, ',', '"' );
        if (count( $header ) == count( $csv_header ) || count( $csv_header ) > count( $header )) {
          foreach ($header as $value) {
            if (!in_array( trim( $value ), $csv_header )) {
              form_set_error( 'file', t( 'Csv file format is wrong as header is not the same.' ) );
            }
          }
        }
        else {
          form_set_error( 'file', t( 'Csv file format is wrong as header is not the same' ) );
        }
        while (($data = fgetcsv( $handle, 0, ',', '"' )) !== FALSE) {
          if ($data[0] == 'permission') {
            continue;
          }
          $modules = array_keys( user_permission_get_modules() );
          if (!in_array( $data[0], $modules )) {
            form_set_error( 'file', t( 'some permissions is not exist in the system, which is mentioned in the csv sheet, please ensure no extra permission should be in the sheet.' ) );
          }
        }
      }
    }
    else {
      drupal_set_message( t( 'There was an error uploading your file. Please try again or contact system administrator.' ), 'error' );
    }
  }
}

/**
 * Submit callback of import csv form.
 *
 * @see permission_csv_export_import_file_validate
 */
function permission_csv_export_import_form_submit($form, $form_state) {
  global $user;
  $roles = user_roles();
  if (!isset( $form_state['values']['process'] )) {
    $uri = permission_csv_export_get_file_uri_from_fid( $form_state['values']['file']['fid'] );
    if (!empty( $uri )) {
      if (file_exists( drupal_realpath( $uri ) )) {
        $operations = [];
        $imported_for = [];
        // Open the csv.
        $handle = fopen( drupal_realpath( $uri ), "r" );
        $header = fgetcsv( $handle, 4096, ',', '"' );
        foreach ($form_state['values']['roles'] as $value) {
          if ($value) {
            $imported_for[] = $value;
          }
        }
        if (!count( $imported_for )) {
          for ($i = 1; $i < count( $header ); $i++) {
            $imported_for[] = array_search( $header[$i], $roles );
          }
        }
        while (($data = fgetcsv( $handle, 0, ',', '"' )) !== FALSE) {
          $csv_data = [];
          if ($data[0] == 'permission') {
            continue;
          }
          $csv_data['permission'] = $data[array_search( 'permission', $header )];
          $role_perm = [];
          for ($i = 1; $i < count( $data ); $i++) {
            $role_perm[array_search( $header[$i], $roles )] = $data[$i];
          }
          $csv_data['rids'] = serialize( $role_perm );
          $operations[] = [
            'permission_csv_export_import_batch_processing',
            [$csv_data],
          ];
        }
        // Once everything is gathered and ready to be processed.
        $batch = [
          'title' => t( 'Importing CSV...' ),
          'operations' => $operations,
          'finished' => 'permission_csv_export_import_finished',
          'error_message' => t( 'The import has encountered an error.' ),
          'progress_message' => t( 'Imported @current of @total rows.' ),
          'file' => drupal_get_path( 'module', 'permission_csv_export' ) . '/includes/import.admin.inc',
        ];
        batch_set( $batch );
        fclose( $handle );
        $imported_for = serialize( $imported_for );
        $data_logs = [
          'fid' => $form_state['values']['file']['fid'],
          'uid' => $user->uid,
          'status' => 0,
          'import_roles' => $imported_for,
          'created' => time(),
          'updated' => time(),
        ];
        drupal_write_record( 'permissions_csv_export_logs', $data_logs );
      }
    }
  }
  else {
    if ($form_state['values']['op'] == 'Discard CSV') {
      permission_csv_export_update_file_status( $form_state['values']['fid'], PERMISSION_CSV_EXPORT_DECLINED );
      db_truncate( 'permissions_csv_export_data' )->execute();
    }
    if ($form_state['values']['op'] == 'Import Permissions') {
      $results = permission_csv_export_get_number_of_results( 'permissions_csv_export_data' );
      for ($i = 0; $i < $results;) {
        $start = $i;
        $i = ($results < ($i + PERMISSION_CSV_EXPORT_IMPORT_PROCESS_LIMIT)) ? $results : $i + PERMISSION_CSV_EXPORT_IMPORT_PROCESS_LIMIT;
        $rows = permission_csv_export_get_results_from_table( 'permissions_csv_export_data', $start, $i );
        $rows['fid'] = $form_state['values']['fid'];
        $operations_permissions[] = [
          'permission_csv_export_process_permissions_batch_processing',
          [$rows],
        ];
      }
      db_truncate( 'permissions_csv_export_last_bakup' )->execute();
      // Create backup of existing permission in the database.
      permission_csv_export_create_backup_of_existing_permissions();
      // Once everything is gathered and ready to be processed.
      $batch = [
        'title' => t( 'Importing Permissions...' ),
        'operations' => $operations_permissions,
        'finished' => 'permission_csv_export_process_permissions_batch_processing_finished',
        'error_message' => t( 'The import permissions has encountered an error.' ),
        'progress_message' => t( 'Imported @current of @total rows.' ),
        'file' => drupal_get_path( 'module', 'permission_csv_export' ) . '/includes/import.admin.inc',
      ];
      batch_set( $batch );
      // Update the file status.
      permission_csv_export_update_file_status( $form_state['values']['fid'], PERMISSION_CSV_EXPORT_PROCESSED );
      db_truncate( 'permissions_csv_export_data' )->execute();
    }
  }
}

/**
 * This function runs the batch processing and creates records in the database.
 *
 * @see permission_csv_export_import_form_submit()
 */
function permission_csv_export_import_batch_processing($data) {
  $data = [
    'permission' => $data['permission'],
    'rids' => $data['rids'],
  ];
  drupal_write_record( 'permissions_csv_export_data', $data );
}

/**
 * This function runs when the batch processing will completed.
 *
 * @see permission_csv_export_import_form_submit()
 */
function permission_csv_export_import_finished() {
  drupal_set_message( t( 'Import Completed Successfully' ) );
}

/**
 * Get allowed header values for csv.
 *
 * @return array
 *   array of csv header.
 */
function permission_csv_export_get_allowed_csv_header() {
  $csv_header = ['permission'];
  $roles = user_roles();
  foreach ($roles as $role) {
    $csv_header[] = $role;
  }

  return $csv_header;
}

/**
 * Helper function to create backup of existing permissions.
 */
function permission_csv_export_create_backup_of_existing_permissions() {
  $roles = user_roles();
  $modules = user_permission_get_modules();
  $role_permission = [];
  $data = [];
  foreach ($roles as $rid => $role) {
    if ($role) {
      $perms = user_role_permissions( [$rid => $rid] );
      permission_csv_export_get_rolewise_permission_access( $perms, $rid, $roles[$rid], $modules, $role_permission );
    }
  }
  permission_csv_export_generate_role_perm_array( $modules, $role_permission, $roles, $data );
  foreach ($data as $perm_array) {
    $operations_backup[] = [
      'permission_csv_export_create_backup_batch',
      [$perm_array],
    ];
  }
  // Once everything is gathered and ready to be processed.
  $batch = [
    'title' => t( 'Creating Backup of existing permissions...' ),
    'operations' => $operations_backup,
    'finished' => 'permission_csv_export_create_backup_batch_finished',
    'error_message' => t( 'The permission backup has encountered an error.' ),
    'progress_message' => t( 'Backup @current of @total rows.' ),
    'file' => drupal_get_path( 'module', 'permission_csv_export' ) . '/includes/import.admin.inc',
  ];
  batch_set( $batch );
}

/**
 * Helper function to rolewise permission array.
 *
 * @param array $modules
 *   Permission array from all active modules.
 * @param array $role_permission
 *   Associative array of rolewise permissions.
 * @param array $roles
 *   Roles array.
 * @param array $data
 *   Data of CSV.
 */
function permission_csv_export_generate_role_perm_array($modules, $role_permission, $roles, &$data) {
  foreach ($modules as $permission => $v) {
    $role_access = [];
    foreach ($roles as $rid => $role) {
      $access = isset( $role_permission[$permission][$role] ) ? 'Y' : 'N';
      $role_access[$rid] = $access;
    }
    $data[] = [
      'permission' => $permission,
      'rids' => serialize( $role_access ),
    ];
  }
}

/**
 * Batch function to create backup of existing permissions.
 *
 * @see permission_csv_export_create_backup_of_existing_permissions()
 */
function permission_csv_export_create_backup_batch($data) {
  $data = [
    'permission' => $data['permission'],
    'rids' => $data['rids'],
  ];
  drupal_write_record( 'permissions_csv_export_last_bakup', $data );
}

/**
 * This function runs when the batch processing will completed.
 *
 * @see permission_csv_export_create_backup_of_existing_permissions()
 */
function permission_csv_export_create_backup_batch_finished() {
  drupal_set_message( t( 'Backup Completed Successfully' ) );
}

/**
 * Batch function to process new CSV and updating permissions.
 *
 * @see permission_csv_export_import_form_submit()
 */
function permission_csv_export_process_permissions_batch_processing($data) {
  if (count( $data )) {
    $fid = $data['fid'];
    unset( $data['fid'] );
    $roles_allowed_for_import = permission_csv_export_get_allowed_roles_for_import( $fid );
    $roles_allowed_for_import = isset( $roles_allowed_for_import ) ? unserialize( $roles_allowed_for_import ) : [];
    $rid_revoke = [];
    $rid_grant = [];
    foreach ($data as $value) {
      $rids = unserialize( $value->rids );
      foreach ($rids as $rid => $access) {
        if (count( $roles_allowed_for_import ) && in_array( $rid, $roles_allowed_for_import )) {
          if ($access == 'Y' || $access == 'y' || $access == 1) {
            $rid_grant[$rid][] = $value->permission;
          }
          if ($access == 'N' || $access == 'n' || $access == 0) {
            $rid_revoke[$rid][] = $value->permission;
          }
        }
      }
    }
    foreach ($rid_revoke as $rid => $permissions) {
      user_role_revoke_permissions( $rid, $permissions );
    }
    foreach ($rid_grant as $rid => $permissions) {
      user_role_grant_permissions( $rid, $permissions );
    }
  }
}

/**
 * This function runs when the batch processing will completed.
 *
 * @see permission_csv_export_import_form_submit()
 */
function permission_csv_export_process_permissions_batch_processing_finished() {
  drupal_set_message( t( 'Permissions imported Successfully' ) );
}

/**
 * Update imported file status.
 *
 * @param int $fid
 *   File fid.
 * @param int $status
 *   File status.
 *
 * @return int
 *   Number of records to be updated.
 */
function permission_csv_export_update_file_status($fid, $status) {
  return db_update( 'permissions_csv_export_logs' )
    ->fields( [
      'status' => $status,
      'updated' => time(),
    ] )
    ->condition( 'fid', $fid, '=' )
    ->execute();
}
