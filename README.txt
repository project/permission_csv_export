CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Manish Kumar <manishupdh900@gmail.com>

This module is basically for import/export of permissions role wise. So 
basically the idea is to export permissions into an CSV and then authorized 
person can make changes directly into CSV and decide permission role wise 
and assign it on CSV itself, Then he can import that CSV and then process 
it at any time, although before importing new permissions it will create a 
backup of last active permissions, so after wrong import you can always 
revert back to the original permissions.

It can also used for migrating permissions to one environment to another, 
it gives flexibility to choose role wise export and import.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Administrator can see can export the csv from 
URL : <yourdomain.com>/admin/config/people/export-permission-csv

Administrator can import new csv from 
URL: <yourdomain.com>/admin/config/people/export-permission-csv/import

Administrator can see import csv logs from
URL: <yourdomain.com>/admin/config/people/export-permission-csv/import-logs
